package ee.srini.proov.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import ee.srini.proov.model.Client;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ClientDaoTests {

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private static final String USERNAME = "admin";
    private static final Long COUNTRY_ID = 1L;
    private Long loginId;

    @Before
    public void setup() {
        loginId = jdbcTemplate.queryForObject("select id from login where username = ?", Arrays.asList(USERNAME).toArray(), Long.class);
    }

    @Test
    public void findByLoginIdTest() {
        List<Client> clients = clientDao.findByLoginId(loginId);

        assertThat(clients).isEmpty();

        Client client = new Client();
        client.setLoginId(loginId);
        client.setCountryId(COUNTRY_ID);
        client.setFirstName("test");
        client.setLastName("test2");
        client.setUsername("username");
        client.setAddress("some address");
        clientDao.add(client);

        Long addedClientId = client.getId();

        clients = clientDao.findByLoginId(loginId);

        assertThat(clients).isNotEmpty();
        assertThat(addedClientId).isEqualTo(clients.get(0).getId());
    }

}
