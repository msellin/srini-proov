INSERT INTO login (username, password) VALUES ('admin', '$2a$10$VOffc3JKgmk/6QAJJPLqz.UfPz8PwSwMBmpQZoIohZiq/uCnJuN8i');
INSERT INTO login (username, password) VALUES ('user1', '$2a$10$go9syvHHqFRQVb4bGAZzwuuGqtdo0pEApIWsnm3rgpPAwhVKLIILK');
INSERT INTO login (username, password) VALUES ('user2', '$2a$10$0sjceD5HfT75iEez6R7pLui/WsJ5t0blJml9NGlg88cxL.Kubpvqq');

INSERT INTO country (name) VALUES ('Estonia');
INSERT INTO country (name) VALUES ('Latvia');
INSERT INTO country (name) VALUES ('Lithuania');
INSERT INTO country (name) VALUES ('Finland');
INSERT INTO country (name) VALUES ('Sweden');
