CREATE TABLE login
(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  username varchar(50) not null,
  password text not null,
  UNIQUE KEY login_username_unique (username)
);

CREATE TABLE country
(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  name text not null
);

CREATE TABLE client
(
  id BIGSERIAL NOT NULL PRIMARY KEY,
  first_name text not null,
  last_name text not null,
  username text not null,
  email text,
  address text not null,
  country_id bigint references country(id),
  login_id bigint references login(id)
);

