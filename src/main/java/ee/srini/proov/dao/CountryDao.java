package ee.srini.proov.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import ee.srini.proov.model.Client;
import ee.srini.proov.model.Country;

@Repository
public class CountryDao {

    private JdbcTemplate jdbcTemplate;
    private BeanPropertyRowMapper mapper = new BeanPropertyRowMapper<>(Country.class);

    public CountryDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Client> findAll() {
        return jdbcTemplate.query("select * from country", mapper);
    }
}
