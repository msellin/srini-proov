package ee.srini.proov.dao;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import ee.srini.proov.model.Client;

@Repository
public class ClientDao {

    private SimpleJdbcInsert simpleJdbcInsert;
    private JdbcTemplate jdbcTemplate;
    private BeanPropertyRowMapper<Client> mapper = new BeanPropertyRowMapper<>(Client.class);

    public ClientDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate).withTableName("client")
                .usingGeneratedKeyColumns("id");
    }

    public List<Client> findByLoginId(Long loginId) {
        String sql = "select * from client where login_id = ?";
        return jdbcTemplate.query(sql, mapper, loginId);
    }

    public Client add(Client client) {
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(client);
        Number newId = simpleJdbcInsert.executeAndReturnKey(parameters);
        client.setId(newId.longValue());
        return client;
    }

    public Client get(Long id) {
        try {
            String sql = "select * from client where id = ?";
            return jdbcTemplate.queryForObject(sql, mapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public void update(Client client) {
        String sql = "update client set first_name = ?, last_name = ?, username = ?, email = ?, address = ?, country_id = ? where id = ?";
        jdbcTemplate.update(sql, client.getFirstName(), client.getLastName(), client.getUsername(), client.getEmail(), client.getAddress(), client.getCountryId(), client.getId());
    }
}
