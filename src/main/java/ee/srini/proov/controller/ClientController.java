package ee.srini.proov.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ee.srini.proov.dao.ClientDao;
import ee.srini.proov.dao.CountryDao;
import ee.srini.proov.model.Client;
import ee.srini.proov.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("clients")
public class ClientController {

    private ClientDao clientDao;
    private CountryDao countryDao;

    public ClientController(ClientDao clientDao, CountryDao countryDao) {
        this.clientDao = clientDao;
        this.countryDao = countryDao;
    }

    @GetMapping
    public String listClients(Model model) {
        Long loginId = SecurityUtil.getCurrentLoginId();
        List<Client> clients = clientDao.findByLoginId(loginId);
        model.addAttribute("clients", clients);
        model.addAttribute("username", SecurityUtil.getCurrentUsername());
        return "index";
    }

    @GetMapping("add")
    public String addClient(Model model) {
        model.addAttribute("client", new Client());
        model.addAttribute("countries", countryDao.findAll());
        model.addAttribute("postUrl", "/clients");
        return "client";
    }

    @GetMapping("{id}")
    @PreAuthorize("hasPermission(#id, 'Client')")
    public String editClient(@PathVariable("id") Long id, Model model) {
        model.addAttribute("client", clientDao.get(id));
        model.addAttribute("countries", countryDao.findAll());
        model.addAttribute("postUrl", "/clients/" + id);
        return "client";
    }

    @PostMapping
    public String addClient(@Valid @ModelAttribute Client client, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("countries", countryDao.findAll());
            model.addAttribute("postUrl", "/clients");
            return "client";
        }

        client.setLoginId(SecurityUtil.getCurrentLoginId());
        clientDao.add(client);

        return "redirect:/";
    }

    @PostMapping("{id}")
    @PreAuthorize("hasPermission(#id, 'Client')")
    public String saveClient(@PathVariable("id") Long id, @Valid @ModelAttribute Client client, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("countries", countryDao.findAll());
            model.addAttribute("postUrl", "/clients/" + id);
            return "client";
        }

        Client existingClient = clientDao.get(id);
        BeanUtils.copyProperties(client, existingClient, "id", "login_id");
        clientDao.update(existingClient);

        return "redirect:/";
    }

}
