package ee.srini.proov.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SecurityUtil {

    public static User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            return (User) auth.getPrincipal();
        }
        return null;
    }

    public static String getCurrentUsername() {
        User user = getCurrentUser();
        if (user != null) {
            return user.getUsername();
        }
        return null;
    }

    public static Long getCurrentLoginId() {
        User user = getCurrentUser();
        if (user != null) {
            SimpleGrantedAuthority authority = (SimpleGrantedAuthority) user.getAuthorities().toArray()[0];
            return Long.valueOf(authority.getAuthority());
        }
        return null;
    }
}
