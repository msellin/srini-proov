package ee.srini.proov.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Client {
    private Long id;
    @Pattern(regexp = "^[A-Za-z]*$")
    @NotBlank(message = "First name is mandatory")
    private String firstName;
    @Pattern(regexp = "^[A-Za-z]*$")
    @NotBlank(message = "Last name is mandatory")
    private String lastName;
    @NotBlank(message = "Username is mandatory")
    private String username;
    @Email
    private String email;
    @NotBlank(message = "Address is mandatory")
    private String address;
    @NotNull(message = "Country must be selected")
    private Long countryId;
    private Long loginId;
}
