package ee.srini.proov.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Country {
    private Long id;
    private String name;
}
