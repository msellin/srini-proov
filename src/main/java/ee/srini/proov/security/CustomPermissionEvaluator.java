package ee.srini.proov.security;

import java.io.Serializable;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import ee.srini.proov.dao.ClientDao;
import ee.srini.proov.model.Client;
import ee.srini.proov.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    private static final String CLIENT = "Client";

    private ClientDao clientDao;

    public CustomPermissionEvaluator(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    @Override
    public boolean hasPermission(Authentication auth, Object targetId, Object targetType) {
        if ((auth == null) || (targetId == null)) {
            return false;
        }
        return hasPrivilege(auth, targetId.toString(), targetType.toString(), null);
    }

    @Override
    public boolean hasPermission(Authentication auth, Serializable targetId, String targetType, Object permission) {
        if ((auth == null) || (targetType == null) || !(permission instanceof String)) {
            return false;
        }
        return hasPrivilege(auth, targetId, targetType.toUpperCase(),
                permission.toString().toUpperCase());
    }

    private boolean hasPrivilege(Authentication auth, Serializable targetId, String targetType, String permission) {
        Long loginId = SecurityUtil.getCurrentLoginId();
        if (CLIENT.equalsIgnoreCase(targetType)) {
            Client client = clientDao.get(Long.valueOf(targetId.toString()));
            return client != null && client.getLoginId().equals(loginId);
        }
        return false;
    }
}
