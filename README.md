# README #

### How to run ###
* Java 8+ needed
* Run either from IDE with 'Run as ...' or from terminal using java -jar ...
* Runs on http://localhost:8088, make sure port is open or switch it in properties file

### Notes ###
* Passwords for all users are same as their usernames
* DB gets resetted with each run/deploy
* Logout can be done via http://localhost:8088/logout (to be able to switch between users)
* No beautiful view for error handling has been implemented (YET?) (e.g. 403 when user has no permission)